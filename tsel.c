/*
 * Copyright (c) 2020 Matthieu Herrb <matthieu@openbsd.org>
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

/* cc -I/usr/X11R6/include -g -Wall tsel.c \
   -L/usr/X11R6/lib -lXt -lXmu -lXaw -lX11 -o tsel */
#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <X11/Xlib.h>
#include <X11/X.h>
#include <X11/Xatom.h>
#include <X11/Intrinsic.h>
#include <X11/StringDefs.h>
#include <X11/Xmu/StdSel.h>
#include <X11/Xmu/Atoms.h>
#include <X11/Xaw/Command.h>


static void
sel_cb(Widget w, XtPointer client_data, Atom *selection, Atom *type,
    XtPointer value, unsigned long *length, int *format);

static void
targets_cb(Widget w, XtPointer client_data, Atom *selection, Atom *type,
    XtPointer value, unsigned long *length, int *format)
{
	Display *dpy = XtDisplay(w);
	Atom target;

	if (*type != XA_ATOM) {
		printf("targets type: %lx\n", (unsigned long)*type);
		return;
	}
	printf("Targets:\n");
	for (int i = 0; i < *length; i++) {
		target = *((Atom *)value + i);
		printf("%s\n", XGetAtomName(dpy, target));
		if (target == XA_DELETE(dpy) || target == XA_TARGETS(dpy))
		    continue;
		XtGetSelectionValue(w, XA_PRIMARY, target,
		    sel_cb, (void *)target,
		    XtLastTimestampProcessed(dpy));
	}
}

static void
sel_cb(Widget w, XtPointer client_data, Atom *selection, Atom *type,
    XtPointer value, unsigned long *length, int *format)
{
	Display *dpy = XtDisplay(w);
	int i;
	char *p = (unsigned char *)value;
	Atom *a = (Atom *)value;


	printf("target %s ", XGetAtomName(dpy,	(Atom)client_data));
	if (*type == 0 || *type == XT_CONVERT_FAIL) {
		printf(": no conversion\n");
		return;
	}
	printf("type %s ", XGetAtomName(dpy, *type));
	printf("length %lu ", *length);
	printf("format %d: ", *format);
	switch (*type) {
	case XA_ATOM:
		for (i = 0; i < *length; i++)
			printf("%s ", XGetAtomName(dpy, a[i]));
		printf("\n");
		break;
	case XA_INTEGER:
		for (i = 0; i < *length; i++)
			printf("%d ", *((int *)value + i));
		printf("\n");
		break;
	case XA_WINDOW:
		for (i = 0; i < *length; i++)
			printf("0x%08x ", *((unsigned int *)value + i));
		printf("\n");
		break;
	case XA_STRING:
		printf("%s\n", (char *)value);
		break;
	default:
		if (*type == XA_UTF8_STRING(dpy))
			wprintf(L"u\"%s\"\n", (char *)value);
		else if (*type == XA_NET_ADDRESS(dpy))
			printf("%u.%u.%u.%u\n", *(unsigned char *)value,
			    *((unsigned char *)value + 1),
			    *((unsigned char *)value + 2),
			    *((unsigned char *)value + 3));
		else {

			for (i = 0; i < *length; i++)
				printf("%02hhx ", p[i]);
			printf("\n");
		}
		break;
	}
}

static void
request_selection(Widget w, XtPointer client_data, XtPointer call_data)
{
	Display *dpy =  XtDisplay(w);

	XtGetSelectionValue(w, XA_PRIMARY, XA_TARGETS(dpy),
	    targets_cb, client_data, XtLastTimestampProcessed(dpy));
}

int
main(int argc, char *argv[])
{
	Widget top, req;

	top = XtInitialize(argv[0], "TSel", NULL, 0, &argc, argv);
	req = XtCreateManagedWidget("Get Selection", commandWidgetClass, top,
	    NULL, 0);

	XtAddCallback(req, XtNcallback, request_selection, NULL);
	XtRealizeWidget(top);
	XtMainLoop();
	printf("done\n");
	return 0;
}
