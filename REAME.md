TSEL - test Xt selection mechanism

Selection in X can be quite complex. In addition to the basic mechanisms
provided by Xlib, the X toolkit added a number of features.

One of the intersting feature is that an application can request 
a number of "targets" from the selection owner. Targets include information
about the selection itself (data format, length, etc...) and information
about the application owning it (name, user, hostname, ip address...). 

Applications need to provide a conversion callback which is responsible 
of providing those information. But not all applications provide the same
targets.

tsel allows to query the targets provided by an application, an then 
for each listed target, to retrieve the selection value associated with 
this target.
